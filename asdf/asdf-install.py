from os import system

def instalar(nome="", versao=""):
    ins = f'asdf install {nome} {versao}'
    system(ins)
    print(f'Instalado: {nome} {versao}')



langs = {
    'crystal': '0.34.0',
    'dart': '2.8.2',
    'dotnet': '3.1.300',
    'erlang': '23.0.1',
    'flutter': '1.17.1-stable',
    'elixir': 'master',
    'go': '1.14.3',
    'haskell': '8.10.1',
    'hub':  'v2.14.2',
    'kotlin': '1.4-M1',
    'node': '12.18.0', 
    'python': '3.8.3',
    'ruby': '2.7.1',
    'rust': 'stable',
    'yarn': '1.22.4'
}

for k, v in langs.items():
    instalar(k, v)
