from os import system
'''

'''
def instalar_plugins(plugin=''):
    system(f'asdf plugin-add {plugin}')



system('git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.8')
system('echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc')
system('echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc')

instalado = input('Fish e Zsh instalados? [Especifique a resposta com fish ou zsh]:  ')

if instalado == 'zsh':
    system('echo ". $HOME/.asdf/asdf.sh" >> ~/.zshrc')
elif instalado == 'fish':
    system('echo "source ~/.asdf/asdf.fish" >> ~/.config/fish/config.fish')
    system('mkdir -p ~/.config/fish/completions')
    system('cp ~/.asdf/completions/asdf.fish ~/.config/fish/completions')
else:
    system('echo "source ~/.asdf/asdf.fish" >> ~/.config/fish/config.fish')
    system('mkdir -p ~/.config/fish/completions')
    system('cp ~/.asdf/completions/asdf.fish ~/.config/fish/completions')
    system('echo ". $HOME/.asdf/asdf.sh" >> ~/.zshrc')



plugins = [
    'ruby https://github.com/asdf-vm/asdf-ruby.git',  
    'python', 
    'nodejs https://github.com/asdf-vm/asdf-nodejs.git && bash ~/.asdf/plugins/nodejs/bin/import-release-team-keyring', 
    'golang https://github.com/kennyp/asdf-golang.git',
    'erlang https://github.com/asdf-vm/asdf-erlang.git',
    'elixir https://github.com/asdf-vm/asdf-elixir.git',
    'kotlin https://github.com/missingcharacter/asdf-kotlin.git',
    'rust https://github.com/code-lever/asdf-rust.git',
    'crystal https://github.com/marciogm/asdf-crystal.git',
    'dotnet-core https://github.com/emersonsoares/asdf-dotnet-core.git',
    'dart https://github.com/patoconnor43/asdf-dart.git',
    'flutter',
    'haskell https://github.com/vic/asdf-haskell.git',
    'hub'
    'yarn'
]

for p in plugins:
    instalar_plugins(p)
