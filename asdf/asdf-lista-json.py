import json
import yaml
from datetime import datetime as dt

langs = ['ruby', 
        'python', 
        'nodejs', 
        'rust', 
        'yarn', 
        'kotlin', 
        'hub', 
        'haskell', 
        'golang', 
        'crystal',
        'dart',
        'dotnet-core',
        'flutter',
        'erlang',
        'elixir'
    ]

paths = ["./asdf/langs/langs.json"]

for l in langs:
    paths.append(f"./asdf/langs/txt/asdf-lista-{l}.txt")

print(paths)

with open(paths[1], 'r') as txt: #Lê um arquivo txt
    file = txt.read()
    
versions = file.split('\n')

ruby = { 
    'ruby': {
        '1': [],
        '2': []
    },
    'maglev': [],
    'jruby': [],
    'mruby': [],
    'rbx': [],
    'ree': [],
    'tru': [],
    'last': []
}


for v in versions:
    if v.split('-')[0].isalpha:
        if v.split('r')[0] == 'j':
            ruby['jruby'].append(v)

        elif v.split('a')[0] == 'm':
            ruby['maglev'].append(v)

        elif v.split('r')[0] == 'm':
            ruby['mruby'].append(v)

        elif v.split('b')[0] == 'r':
            ruby['rbx'].append(v)

        elif v.split('-')[0] == 'ree':
            ruby['ree'].append(v)

        elif v.split('r')[0] == 't':
            ruby['tru'].append(v)

    if v.split('.')[0] == '1':
        ruby['ruby']['1'].append(v)

    elif v.split('.')[0] == '2':
        ruby['ruby']['2'].append(v)



ruby['last'].append(ruby['ruby']['1'][-1])
ruby['last'].append(ruby['ruby']['2'][-1])
ruby['last'].append(ruby['tru'][-1])
ruby['last'].append(ruby['ree'][-1])
ruby['last'].append(ruby['rbx'][-1])
ruby['last'].append(ruby['mruby'][-1])
ruby['last'].append(ruby['maglev'][-1])
ruby['last'].append(ruby['jruby'][-1])


with open(paths[2], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

# activepython
# anaconda
# anaconda3
# ironpython
# jython
# micropython
# miniconda
# miniconda3
# pypy
# pypy2
# pypy3
# stackless

python = {
    'python': {
        '2': [],
        '3': []
    },
    'act': [],
    'conda': {
        'conda': [],
        '2': [],
        '3': []
    },
    'iron': [],
    'jy': [],
    'micro': [],
    'miniconda': {
        'mini': [],
        '2': [],
        '3': []
    },
    'pypy': {
        'py': [],
        '2': [],
        '3': []
    },
    'stack': [],
    'last': []
}

for v in versions:
    if v.split('.')[0] == '2':
       python['python']['2'].append(v)

    elif v.split('.')[0] == '3':
       python['python']['3'].append(v)
    
    elif 'anaconda' in v:
        if '3' in v.split('-')[0]:
           python['conda']['3'].append(v)
        
        elif '2' in v.split('-')[0]:
            python['conda']['2'].append(v)
        
        else:
            python['conda']['conda'].append(v)
            
    
    elif 'activepython' in v:
            python['act'].append(v)
    
    elif 'ironpython' in v:
        python['iron'].append(v)
    
    elif 'jython' in v:
        python['jy'].append(v)

    elif 'micro' in v:
        python['micro'].append(v)

    elif 'miniconda' in v:
        if '2' in v.split('-')[0]:
            python['miniconda']['2'].append(v)
        elif '3' in v.split('-')[0]:
           python['miniconda']['3'].append(v)
        else:
            python['miniconda']['mini'].append(v)

    elif 'pypy' in v:
        if '2' in v.split('-')[0]:
            python['pypy']['2'].append(v)
        elif '3' in v.split('-')[0]:
           python['pypy']['3'].append(v)
        else:
            python['pypy']['py'].append(v)
    
    elif 'stack' in v:
        python['stack'].append(v)



last = [    
        python['python']['2'][-1],
        python['python']['3'][-4],
        python['act'][-1],
        python['conda']['conda'][-1],
        python['conda']['2'][-1],
        python['conda']['3'][-1],
        python['iron'][-1],
        python['jy'][-1],
        python['micro'][-1],
        python['miniconda']['mini'][-1],
        python['miniconda']['2'][-1],
        python['miniconda']['3'][-1],
        python['pypy']['py'][-1],
        python['pypy']['2'][-1],
        python['pypy']['3'][-1],
        python['stack'][-1]
        ]

python['last'] = last

with open(paths[3], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

versions = versions[1:-2]

node = {
    'lts': [],
    'current': [],
    'last': []
}

for v in versions:
    if int(v.split('.')[0]) == 12:
        node['lts'].append(v)
    elif int(v.split('.')[0]) == 13 or int(v.split('.')[0]) == 14:
        node['current'].append(v)


last = [node['lts'][-1], node['current'][-1]]

node['last'] = last

with open(paths[4], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

rust = {
    'named': [],
    'number': []
}

for v in versions:
    if 'nightly' in v or 'beta' in v or 'stable' in v:
        rust['named'].append(v)
    elif '1' == v.split('.')[0] or '2' == v.split('.')[0]:
        rust['number'].append(v)

with open(paths[5], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

yarn = {
    'versions': [],
    'last': []
}

for v in versions:
    if '1' == v.split('.')[0] or '2' == v.split('.')[0]:
        yarn['versions'].append(v)


with open(paths[6], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

yarn['last'] = yarn['versions'][-1]

kotlin = {
    'versions': [],
    'last': []
}

for v in versions:
    if '1' == v.split('.')[0] or '2' == v.split('.')[0]:
        kotlin['versions'].append(v)

kotlin['last'] = kotlin['versions'][-1]

with open(paths[7], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

hub = {
    'versions': [],
    'last': []
}

for v in versions:
    if 'v' in v.split('.')[0]:
        hub['versions'].append(v)

hub['last'] = hub['versions'][-1]

with open(paths[8], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

haskell = {
    'versions': [],
    'last': []
}

c = 0

for v in versions[1:]:
    c += 1
    if c == (len(versions) - 2):
        break

    if v.isnumeric:
        haskell['versions'].append(v)

haskell['last'] = haskell['versions'][-1]


with open(paths[9], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

go = {
    'versions': [],
    'last': []
}


for v in versions:
    if '1' == v.split('.')[0] or '2' == v.split('.')[0]:
        go['versions'].append(v)

go['last'] = go['versions'][-1]

with open(paths[10], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

crystal = {
    'versions': [],
    'last': []
}


for v in versions:
    if '0' == v.split('.')[0] or '1' == v.split('.')[0]:
        crystal['versions'].append(v)

crystal['last'] = crystal['versions'][-1]


with open(paths[11], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

dart = {
    'versions': [],
    'last': []
}


for v in versions:
    
    if '0' == v.split('.')[0] or '1' == v.split('.')[0] or '2' == v.split('.')[0] or '3' == v.split('.')[0]:
        dart['versions'].append(v)

dart['last'] = dart['versions'][-1]


with open(paths[12], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

dotnet = {
    'versions': [],
    'last': []
}


for v in versions[1:-2]:
    dotnet['versions'].append(v)

dotnet['last'] = dotnet['versions'][-1]


with open(paths[13], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

flutter = {
    'versions': [],
    'last': []
}

for v in versions[1:-2]:
    flutter['versions'].append(v)

flutter['last'] = flutter['versions'][-1]


with open(paths[14], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

erlang = {
    'versions': [],
    'last': []
}

for v in versions[10:-2]:
    erlang['versions'].append(v)

erlang['last'] = erlang['versions'][-1]


with open(paths[15], 'r') as txt: 
    file = txt.read()
    
versions = file.split('\n')

elixir = {
    'versions': [],
    'last': []
}

for v in versions[10:-2]:
    print(v)
    elixir['versions'].append(v)

elixir['last'] = elixir['versions'][-1]



js = {
     'ruby': ruby,
     'python': python,
     'node': node,
     'rust': rust,
     'yarn': yarn,
     'kotlin': kotlin,
     'hub': hub,
     'haskell': haskell,
     'go': go,
     'crystal': crystal,
     'dart': dart,
     'dotnet': dotnet,
     'flutter': flutter,
     'erlang': erlang,
     'elixir': elixir,
     'data': f'{dt.now()}'
}

with open(paths[0], 'w') as json_file:
    json.dump(js, json_file, indent=4)

with open('./asdf/langs/langs.yaml', 'w') as f:
    data = yaml.dump(js, f)
    print('arquivo criado')
