from datetime import datetime
# Importar modulo do sistema operacional
import os

def asdf(nome="node"):
    
    path = f'./asdf/langs/txt/asdf-lista-{nome}.txt'
    nomeUpper = nome.upper()
    os.system(f"echo '## {nomeUpper} ##' >> {path}")
    os.system(f"asdf list-all {nome} >> {path}")

    data = f"{datetime.now()}"
    os.system(f'echo {data} >> {path}')
    


# Main
langs = ['ruby', 
        'python', 
        'nodejs', 
        'rust', 
        'yarn', 
        'kotlin', 
        'hub', 
        'haskell', 
        'golang', 
        'crystal',
        'dart',
        'dotnet-core',
        'flutter',
        'erlang',
        'elixir'
    ]

for l in langs:
    asdf(l)



